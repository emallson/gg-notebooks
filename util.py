import json
import numpy as np
import pandas as pd
from cles.cles import cles as cles_orig

def read_df(datafiles):
    events = []
    for path in datafiles:
        with open(path) as file:
            for line in file:
                events.append(json.loads(line))
    df = pd.io.json.json_normalize(events)
    df['created_at'] = pd.to_datetime(df['created_at'])
    df = df.set_index('created_at')
    df.sort_index(inplace=True)
    return df

def filter_spam(df, quantile=0.02, lev_thresh=None, htag_thresh=None):
    "removes spam using similar metrics to chatzakou. condensed version based on the Spam? notebook"
    import jellyfish as jf
    def remove_entities(row):
        mask = np.ones_like(list(row['text']), dtype=np.bool)
        entity_keys = ['hashtags', 'media', 'symbols', 'trends', 'urls', 'user_mentions']
        for ek in entity_keys:
            if 'entities.' + ek not in row or not type(row['entities.' + ek]) == list:
                continue
            for ent in row['entities.' + ek]:
                ind = ent['indices']
                mask[np.arange(*ind)] = False
        return ''.join(np.array(list(row['text']))[mask])
    
    def mean_pairwise_lev(df, samples=100):
        if len(df) < samples:
            samples = len(df)
        texts = df.sample(samples)
        return np.mean([jf.levenshtein_distance(a, b) / max(len(a), len(b)) for i, a in enumerate(texts[:-1]) if len(a) > 0 for b in texts[i+1:] if len(b) > 0])

    df['clean_text'] = df.apply(remove_entities, axis=1)
    mean_lev = df.groupby('user.id').filter(lambda df: len(df) > 1).groupby('user.id')['text'].agg(mean_pairwise_lev, samples=100)
    if lev_thresh is None:
        lev_ids = mean_lev[mean_lev < mean_lev.quantile(quantile)].index
    else:
        lev_ids = mean_lev[mean_lev < lev_thresh].index
    df['hashtag_count'] = df['entities.hashtags'].apply(lambda hts: len(hts) if type(hts) == list else 0)
    mean_htags = df.groupby('user.id').filter(lambda df: len(df) > 1).groupby('user.id')['hashtag_count'].mean() 
    if htag_thresh is None:
        htag_ids = mean_htags[mean_htags > mean_htags.quantile(1 - quantile)].index
    else:
        htag_ids = mean_htags[mean_htags > htag_thresh].index
    if htag_thresh is None and lev_thresh is None:
        print(mean_lev.quantile(quantile), mean_htags.quantile(1 - quantile))
    df = df.drop('clean_text', axis=1).drop('hashtag_count', axis=1)
    return df[~df['user.id'].isin(lev_ids) & ~df['user.id'].isin(htag_ids)]

def cles(X, Y, alt):
    """Wrapper for the CLES code by ajschumacher that makes the argument order match that of `scipy.stats.mannwhitneyu`."""
    if alt == 'greater':
        # hypothesis: X > Y
        return cles_orig(Y, X)
    elif alt == 'less':
        # hypothesis: X < Y
        return cles_orig(X, Y)
    else:
        raise ValueError('Cannot automatically compute CLES for alternative hypothesis {}'.format(alt))
        
def rbcorr(X, Y, alt='greater'):
    """Compute the rank-biserial correlation using the simple difference formula given in Kerby (2014). Alternative hypothesis is that X > Y by default (other value supported is 'less')."""
    from bisect import bisect_left
    if alt == 'less':
        X, Y = Y, X
    X, Y = sorted(X), sorted(Y)
    P, Q = 0, 0
    for x in X:
        idx = bisect_left(Y, x) # returns the index at which one should insert x s.t. all(Y[:idx] < x) and all(Y[idx:] >= x)
        assert (idx == len(Y) and Y[-1] < x) or (idx == 0 and Y[0] >= x) or (Y[idx-1] < x and Y[idx] >= x), '{} {} {}'.format(x, Y[idx-1], Y[idx])
        P += idx
        while idx < len(Y) and Y[idx] == x:
            idx += 1
        Q += len(Y) - idx
    Pmax = len(X) * len(Y)
    return float(P) / Pmax - float(Q) / Pmax

class EffectSize:
    def __init__(self, cles, rbc, alt=None):
        self.cles = cles
        self.rbc = rbc
        self.alt = alt
    
    def __str__(self):
        return 'CLES: {}, RB Corr: {}'.format(self.cles, self.rbc)
    
    def __repr__(self):
        return "'" + str(self) + "'"

def effect_size(X, Y, alt='greater'):
    return EffectSize(cles(X, Y, alt), rbcorr(X, Y, alt), alt)

def phi_coeff(C):
    return (np.prod(np.diag(C)) - np.prod(np.diag(np.fliplr(C)))) / np.sqrt(np.prod(np.sum(C, axis=0)) * np.prod(np.sum(C, axis=1)))