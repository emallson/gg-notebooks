""" Set of functions for the community black-box tests. Separated from
the main notebooks so it can be called from multiple notebooks easily.
(I use a lot of throwaway notebooks.) """
from util import *
import numpy as np
import pandas as pd
from scipy import stats
from matplotlib import pyplot as plt

DATA_PATH = __import__('repro-lamba', globals(), locals(), ['OUTPUT_PATH'], 0).OUTPUT_PATH

def data_for(kind, key, date):
    """Load data for the specified event. Ex:

    >>> data_for('hashtag', 'muslimrage', '2012-09-17')
    """
    return filter_spam(read_df([DATA_PATH.format(date=date, kind=kind, key=key)]), lev_thresh=0.7, htag_thresh=2)

def random_group(df, k):
    """Returns a random set of user ids of size `k` from the given dataframe."""
    return np.random.choice(df['user.id'].unique(), k)

def random_group_like(df, group):
    """Returns a random group of user ids of the same size as `group`."""
    return random_group(df, len(group))

def edgeset(df, retweets=False):
    return pd.DataFrame([(entry['id'], entry['user.id'], mention['id']) for idx, entry in df.iterrows()
             for mention in entry['entities.user_mentions'] if retweets or np.isnan(entry['retweeted_status.user.id'])], columns=['id', 'user.id', 'mention.id'])

# this is the old definition of edgeset that I used when starting this
# study. it will count mentions multiple times if they are in RTs of the
# form `RT @user: <text> @mention <text>` when counting number of times
# users mentioned or were mentioned, it generally makes sense to exclude
# these RT-mentions.  However, doing so here ends up swapping out a
# couple of interesting users (like Randi Harper) for uninteresting
# users (like YouTube).  The majority of the list stays the same, so I'm
# just going to leave it alone.
def edgeset_retweeted(df, retweets=False):
    return pd.DataFrame([(entry['id'], entry['user.id'], mention['id']) for idx, entry in df.iterrows()
             for mention in entry['entities.user_mentions'] if retweets or entry['retweeted_status.user.id'] != mention['id']], columns=['id', 'user.id', 'mention.id'])

# these accounts will not be included in the notable list. if they are,
# instead an EMPTY SPOT will fill their place.
BLACKLIST = set([
    807095, # nytimes
    813286, # BarackObama
    1367531, # FoxNews
    10228272, # YouTube
    11134252, # GOP
    12925072, # TwitPic
    14116807, # ShareThis
    87818409, # guardian
    759251, # CNN
    972651, # Mashable
    11522502, # Etsy
    15923226, # CSPAN
    15164565, # Slate
    16955991, # Salon
])

def notables(df, count=30):
    mrdf = edgeset_retweeted(df)
    mrdf.set_index('id', inplace=True)
    return [notable for notable in mrdf.groupby('mention.id')['user.id'].count().sort_values().iloc[-30:].index if notable not in BLACKLIST]

def notable_group(df, notables): 
    return df[df['retweeted_status.user.id'].isin(notables)]['user.id'].unique()

def plot_in_mention_ratio(df, mdf, group, ax=None, label=None):
    mentions = mdf[mdf['user.id'].isin(group) & mdf['mention.id'].isin(group)]
    all_mentions = mdf[mdf['user.id'].isin(group)]
    df.groupby(pd.TimeGrouper('1d'))['id'].agg(lambda ser: np.count_nonzero(ser.isin(mentions.index)) / np.count_nonzero(ser.isin(all_mentions.index))).fillna(0).plot(ax=ax, label=label if label is not None else df.name)
    if ax is None:
        plt.legend()
        plt.xlabel('Time')
        plt.ylabel('Ratio of In-Group Mentions per Day')
        plt.ylim([0,1])
        plt.show()
        
def plot_conductance(df, mdf, group, ax=None, label=None):
    out_mentions = mdf[mdf['user.id'].isin(group) & ~mdf['mention.id'].isin(group)]
    all_mentions = mdf[mdf['user.id'].isin(group)]
    df.groupby(pd.TimeGrouper('1d'))['id'].agg(lambda ser: np.count_nonzero(ser.isin(out_mentions.index)) / np.count_nonzero(ser.isin(all_mentions.index))).fillna(0).plot(ax=ax, label=label if label is not None else df.name)
    if ax is None:
        plt.legend()
        plt.xlabel('Time')
        plt.ylabel('Conductance per Day')
        plt.ylim([0,1])
        plt.show()

def mdf(df):
    es = edgeset(df)
    return es.set_index('id')

def test_vs_random(df, mdf, group, rgroup):
    """Tests whether users within `group` mentioned one another at a
    higher rate than a random group `rgroup` via the χ² test. 
    
    The `df` parameter is used to restrict the scope of the test to
    smaller intervals.
    
    Contingency Table is of the form:

                | In-Group | Out-Group |
      Selected  | .......  | ........  |
      Random    | .......  | ........  |
    """
    men_pp = mdf[mdf['user.id'].isin(group) & mdf['mention.id'].isin(group)]
    men_pn = mdf[mdf['user.id'].isin(group) & ~mdf['mention.id'].isin(group)]

    men_np = mdf[mdf['user.id'].isin(rgroup) & mdf['mention.id'].isin(rgroup)]
    men_nn = mdf[mdf['user.id'].isin(rgroup) & ~mdf['mention.id'].isin(rgroup)]

    contingency = np.zeros((2,2))
    contingency[0,0] = np.count_nonzero(df['id'].isin(men_pp.index))
    contingency[0,1] = np.count_nonzero(df['id'].isin(men_pn.index))
    contingency[1,0] = np.count_nonzero(df['id'].isin(men_np.index))
    contingency[1,1] = np.count_nonzero(df['id'].isin(men_nn.index))

    chi2, p, dof, exp = stats.chi2_contingency(contingency)

    return {
        'kind': 'chi2',
        'stat': {'pvalue': p, 'statistic': chi2, 'dof': dof},
        'effect_size': {'phi': phi_coeff(contingency)},
        'table': contingency,
    }

def test_growth(df1, df2, mdf, group):
    """Tests for the presence of growth in a group's in-group mentions
    between two intervals (specified by df1 and df2) via the χ² test.
    
    Contingency Table is of the form:

                 | In-Group | Out-Group |
      Interval 2 | .......  | ........  |
      Interval 1 | .......  | ........  |
    """
    men_in = mdf[mdf['user.id'].isin(group) & mdf['mention.id'].isin(group)]
    men_out = mdf[mdf['user.id'].isin(group) & ~mdf['mention.id'].isin(group)]

    contingency = np.zeros((2,2))
    contingency[0,0] = np.count_nonzero(df2['id'].isin(men_in.index))
    contingency[0,1] = np.count_nonzero(df2['id'].isin(men_out.index))
    contingency[1,0] = np.count_nonzero(df1['id'].isin(men_in.index))
    contingency[1,1] = np.count_nonzero(df1['id'].isin(men_out.index))

    chi2, p, dof, exp = stats.chi2_contingency(contingency)

    return {
        'kind': 'chi2',
        'stat': {'pvalue': p, 'statistic': chi2, 'dof': dof},
        'effect_size': {'phi': phi_coeff(contingency)},
        'table': contingency,
    }
